# Purr-Data Vintage Platform Audio Emulation

## Motivation and Description

During the summer of 2023, I participated in Google Summer of Code where I spent approximately five months working on a vintage game audio emulator in the graphical programming language Purr Data. This large-sized project was one of the pre-existing project ideas listed on the official [purr-data gitlab repo](https://git.purrdata.net/jwilkes/summer-of-code-ideas-list) and I, a child of the '90s, was happy to oblige. The two game systems I chose to emulate are the SNES and the Sega Genesis.

## Features

- Six-voice FM synth (with 4 additional pulse-wave voices)
- Eight-voice wavetable synth
- Qlist-based sequencer
- Midi functionality
- Help files for both synths and the sequencer

## Getting Started

### GEMS

![GEMS image](https://gitlab.com/brittneyjuliet/purr-data-vintage-platform-audio-emulation/-/raw/de0b65548ee8b84d6310e05cb9c880f1a8fb82ed/images/gems.png)

Upon opening the patch, GEMS - Genesis Editor for Music and Sound Effects - (named after Jonathan Miller, Burt Sloane, Chris Grigg, and Mark Miller's software of the same name ) is initialized with each of the six fm voices turned on and ready for midi input. The additional pulse voices must be sequenced via the included sequencer. By clicking on one of the six toggles above the algorithm reference image, the parameters for each voice of the synth can be modified and adjusted (including the algorithm for each voice). Further controls include a single LFO for all voices, left and right panning for the fm voices, and a mixer for volume control of each voice.

### SNES

![SNES image](https://gitlab.com/brittneyjuliet/purr-data-vintage-platform-audio-emulation/-/raw/de0b65548ee8b84d6310e05cb9c880f1a8fb82ed/images/snes.png)

The SNES patch requires a bit of initialization before it's ready to play. Users must select a soundfont via the dropdown menu as well as adjust the volume for each voice before the synth can be heard. Once soundfonts are selected and volume is adjusted, similarly to the GEMS emulator, the SNES emulator can be performed with a midi controller with no adjustment of parameters. As of now, noise is only available via the sequencer, but that will be addressed in a future update. The SNES emulator consists of typical synth parameters, such as envelope generatos, delay (echo), feedback (reverb) and a filter (that is only applied to sounds that have been processed by feedback), in addition to fine and coarse tuning. A very fun but totally unexpected bonus parameter is single-voice fm. Each voice can act as a modulator to modulate the following voice, i.e. voice number one (modulator) can modulate voice number two (carrier). The volume of the modulator affects the amount of modulation that is applied to the carrier. It should be noted that the modulator can still act as an active voice regardless of fm is on or off.

## Sequencer

![Sequencer image](https://gitlab.com/brittneyjuliet/purr-data-vintage-platform-audio-emulation/-/raw/main/images/sequencer.png?ref_type=heads)

The sequencer comes preloaded with a short demo for both the GEMS and SNES emulators ("Secret of the Forest," composed by Yasunori Mitsuda). The UI of the interface allows the user to play, stop, and loop the selected sequence. Additionally, a 'dummy' sequence has been added - my_sequence - that allows users to input their own sequence into qlist. See sequencer-help.pd for more information on sequencing. As of now, the sequencer can indepedently control 21 GEMS parameters and 24 SNES parameters.

## Future Work

The most pressing issue I'd like to address is the SNES filter. The coefficients aren't quite right, and so the filter isn't behaving as expected. I plan on using Pd's built-in filters for now, until I can accurately implement the "true" SNES filter. Additionally, I'd like to try and implement an interactive gui for the GEMS emulator whereby the user can click on an algorithm (on the included png) to select each voice's operator routing.

## Acknowledgements

A huge thank you to my GSoC mentors Jonathan Wilkes, Albert Graef, and Matt Barber! I had a blast working with them and learned so much. I'm incredibly thankful for the opportunity :)